Running :
    
    - clone
    - composer
    - symfony permissions
    - Database
        - connect to your db
            - php bin/console doctrine:migrations:migrate
            - php bin/console doctrine:fixtures:load
        - or connect with sqlitef with this test database
            - cp data.db var/
    - php bin/console server:start



Write notes about each step of the progress, your observations and what you did (extra)
    
    -   Adding Authentication
    -   Adding some bootstrap style
    -   Adding Factories
    -   Adding product crud
    -   Abstracting Types
    -   Adding cart and types using previous type abstracting layer
    -   Adding relations between cart and product
    -   Adding relations between cart and user
    -   Managing carts for guests
    
Write about obstacles faced you (extra)
    
    -   Moving form Laravel to Symfony
    -   Moving from Blade to twig
    -   Moving from Eloquent to Doctrine
    -   Selecting the best design pattern
    -   Handling symfony sessions 
    
Write about scenarios you had followed till finding your path (extra)

    -   Watching some videos
    -   reading some articles
    -   Studing some umls
    
To add type of products or carts
    
    -   Create New Class of this type
    -   Extend the equivalent Entity Product or Cart
    -   Assign this class ( id , class name , fornt-end name ) 
            in the equivalent interface
    