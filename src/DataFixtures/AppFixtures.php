<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Factories\ProductFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $faker = Factory::create();
        \Bezhanov\Faker\ProviderCollectionHelper::addAllProvidersTo($faker);


        for ($i= 0 ; $i < 16 ; $i ++) {
            $array_rand = array_rand(Product::AVAILABLE_TYPES);
            $type = Product::AVAILABLE_TYPES[$array_rand];
            $product = (new ProductFactory())->create($type);
            $product->setPrice($faker->randomFloat(2, 111, 999));
            $product->setName($faker->productName);
            $manager->persist($product);
            $manager->flush();
        }

    }
}
