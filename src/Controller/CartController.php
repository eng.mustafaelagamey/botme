<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Product;
use App\Factories\CartFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cart")
 */
class CartController extends AbstractController
{

    /**
     * ********** Displaying the cart **********
     * @Route("/{cart_type}/show", name="my_cart", methods={"GET"})
     * @param $cart_type
     * @return Response
     */
    public function myCart($cart_type): Response
    {
        $cart = $this->resolveCartObject($cart_type);
        return $this->render('cart/user_cart.html.twig', [
            'cart' => $cart,
        ]);
    }

    /**
     * ********** Adding product to cart **********
     * @Route("/{cart_type}/{product}/Add", name="my_cart_add_product", methods={"GET"})
     * @param $cart_type
     * @param Product $product
     * @return Response
     */
    public function myCartAdd($cart_type, Product $product): Response
    {
        
        $cart = $this->resolveCartObject($cart_type);
        $cart->addProduct($product);
        $this->saveCart($cart);
        $this->addFlash('success', 'Product added!');
        return $this->redirectToRoute('product_index');
    }

    /**
     * ********** Removing product from cart **********
     * @Route("/{cart_type}/{product}/remove", name="my_cart_remove_product", methods={"DELETE"})
     * @param $cart_type
     * @param Product $product
     * @return Response
     */
    public function myCartRemove($cart_type, Product $product): Response
    {
        $cart = $this->resolveCartObject($cart_type);
        $cart->removeProduct($product);
        $this->saveCart($cart);
        $this->addFlash('danger', 'Product removed!');
        return $this->redirectToRoute('my_cart', [
            'cart_type' => $cart_type
        ]);
    }

    /**
     * ********** Emptying cart **********
     * @Route("/{cart_type}/empty", name="my_cart_empty", methods={"DELETE"})
     * @param $cart_type
     * @return Response
     */
    public function myCartEmpty($cart_type): Response
    {
        $cart = $this->resolveCartObject($cart_type);
        $cart->removeAllProducts();
        $this->saveCart($cart);
        $this->addFlash('danger', 'Cart emptied!');
        return $this->redirectToRoute('my_cart', [
            'cart_type' => $cart_type
        ]);

    }


    /**
     * ********** Saving cart **********
     * @param $cart
     */
    private function saveCart($cart)
    {
        if ($user = $this->getUser()) {
            $user->addCart($cart);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($cart);
            $entityManager->flush();
            return $cart;
        }
        if ($this->container->get('session')->isStarted()) {
            $session = new Session(new PhpBridgeSessionStorage());
            $session->start();

        } else {
            $session = new Session();
        }
        $key = $this->getSessionKey(get_class($cart));

        return $session->set($key, $cart);
    }

    /**
     * ********** Helper to get session cart key **********
     * @param $requestedType
     * @return string
     */
    private static function getSessionKey($requestedType): string
    {
        return 'cart.' . $requestedType;
    }

    /**
     * ********** Get Cart from passed type **********
     * @param $cart_type
     * @return Cart|mixed|null
     */
    private function resolveCartObject($cart_type)
    {
        $resolvedType = Cart::resolveType($cart_type);
        $typeClass = Cart::typeClass($resolvedType);
        $cart = $this->resolveCart($typeClass);
        dump($typeClass);

        if (!$cart) {
            $cartFactory = new CartFactory();
            $cart = $cartFactory->create($typeClass);
        }
        return $cart;
    }

    /**
     * ********** Resolving cart db for users and session for guests **********
     * @param $requestedType
     * @return Cart
     */
    private function resolveCart($requestedType): ?Cart
    {
        if ($user = $this->getUser()) {
            return $cart = $user->findCartByClassType($requestedType) ?: null;
        }
        return $cart = $this->resolveSessionCart($requestedType);
    }


    /**
     * ********** Get cart from session **********
     * @param $requestedType
     * @return Cart
     */
    private function resolveSessionCart($requestedType): ?Cart
    {
        // Get Symfony to interface with this existing session
        if ($this->container->get('session')->isStarted()) {
            $session = new Session(new PhpBridgeSessionStorage());
            $session->start();
        } else {
            $session = new Session();
        }
        $key = $this->getSessionKey($requestedType);
        if ($session->has($key)) {
            return $session->get($key);
        }
        return null;
    }

}
