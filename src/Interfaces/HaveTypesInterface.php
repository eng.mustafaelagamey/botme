<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/22/19
 * Time: 11:24 PM
 */

namespace App\Interfaces;


interface HaveTypesInterface
{

    public static function defaultType():string ;

    public static function resolveType($value):string ;

    public static function typeClass($value):string ;

    public static function getTypeName($string):string ;

}