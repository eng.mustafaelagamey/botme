<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:20 PM
 */

namespace App\Interfaces;


use App\Entity\ShoppingCart;
use App\Entity\WishCart;

interface CartInterface extends HaveTypesInterface
{
    // current products
    public const SHOPPING_CART = 0;
    public const WISH_CART = 1;



    // mapping for using in single orm single table inheritance and in code
    public const AVAILABLE_TYPES = [
        self::SHOPPING_CART => ShoppingCart::class,
        self::WISH_CART => WishCart::class,
    ];


    // used to mapping names to generate choices for form type and type name
    public const TYPES_NAMES = [
        self::SHOPPING_CART => 'Shopping Cart',
        self::WISH_CART => 'Wish Cart',
    ];

}