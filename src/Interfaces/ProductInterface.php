<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:20 PM
 */

namespace App\Interfaces;


use App\Entity\NormalProduct;
use App\Entity\SaleProduct;

interface ProductInterface extends HaveTypesInterface
{
    // current products
    public const NORMAL_PRODUCT = 0;
    public const SALE_PRODUCT = 1;


    // mapping for using in single orm single table inheritance and in code
    public const AVAILABLE_TYPES = [
        self::NORMAL_PRODUCT => NormalProduct::class,
        self::SALE_PRODUCT => SaleProduct::class,
    ];


    // used to mapping names to generate choices for form type and type name
    public const PRODUCTS_NAMES = [
        self::NORMAL_PRODUCT => 'Normal Product',
        self::SALE_PRODUCT => 'Sale Product',
    ];

}