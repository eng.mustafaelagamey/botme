<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:23 PM
 */

namespace App\Abstracts;


abstract class ObjectFactory
{
    abstract public function create($type,array $options = []);
}