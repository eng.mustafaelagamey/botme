<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:17 PM
 */

namespace App\Traits;


trait HaveTypesTrait
{


    public static function defaultType():string
    {

        // for php >= 7.3
//        return array_key_first(self::AVAILABLE_TYPES);

        $array = self::AVAILABLE_TYPES;
        reset($array);
        return key($array);
    }

    public static function typeClass($value):string
    {
        return self::AVAILABLE_TYPES[$value] ?? self::AVAILABLE_TYPES[self::defaultType()];
    }


    public static function getTypeName($string):string
    {
        return self::TYPES_NAMES[$string];
    }

    public static function resolveType($type): string
    {
        return  $type ?? self::defaultType();
    }


}