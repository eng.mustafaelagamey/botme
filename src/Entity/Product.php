<?php

namespace App\Entity;

use App\Interfaces\ProductInterface;
use App\Traits\HaveTypesTrait;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap(ProductInterface::AVAILABLE_TYPES)
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
abstract class Product implements ProductInterface
{
    protected $type = Product::NORMAL_PRODUCT;

    use HaveTypesTrait;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getType()
    {
        return ProductInterface::PRODUCTS_NAMES[$this->type];
    }

    public function getTypeValue()
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }


}
