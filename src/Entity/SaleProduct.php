<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:28 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class SaleProduct extends NormalProduct
{
    protected $type =Product::SALE_PRODUCT;

}