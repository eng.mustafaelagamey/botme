<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:27 PM
 */

namespace App\Entity;

use App\Interfaces\CartInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class WishCart extends Cart
{
    protected $type =self::WISH_CART;
}