<?php

namespace App\Entity;

use App\Interfaces\CartInterface;
use App\Traits\HaveTypesTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap(CartInterface::AVAILABLE_TYPES)
 * @ORM\Entity(repositoryClass="App\Repository\CartRepository")
 */
abstract class Cart implements CartInterface
{
    protected $type = self::SHOPPING_CART;
    use HaveTypesTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product",fetch="EAGER")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="carts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType()
    {
        return self::TYPES_NAMES[$this->type];
    }

    public function getTypeValue()
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->checkProductExist($product)) {
            $this->products->add($product);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->checkProductExist($product)) {
            $this->removeProductFromCart($product);
        }

        return $this;
    }

    public function removeAllProducts(): self
    {
        $this->products->clear();
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Generating Total value
     * @return mixed
     */
    public function totalValue()
    {

        return array_reduce($this->getProducts()->toArray(),
            function ($carry, Product $product) {
                $carry += $product->getPrice();
                return $carry;
            });
    }

    /**
     * as the product created in session not like created in db
     * we have to improve the search function
     * @param Product $product
     * @return bool
     */
    public function checkProductExist(Product $product): bool
    {

        if ($this->getUser()) {
            return $this->products->contains($product);
        }

        return !$this->products->filter(function (Product $cartProduct) use ($product) {
            return $cartProduct->getId() == $product->getId();
        })->isEmpty();


    }

    /**
     *
     * @param Product $product
     * @return bool
     */
    private function removeProductFromCart(Product $product): bool
    {
        if ($this->getUser()) {
            return $this->products->removeElement($product);

        }
        $this->products = $this->products->filter(
            function (Product $cartProduct) use ($product) {
                return $cartProduct->getId() != $product->getId();
            });
        return true;
    }


}
