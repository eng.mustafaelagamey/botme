<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/21/19
 * Time: 6:56 PM
 */

namespace App\Factories;


use App\Abstracts\ObjectFactory;
use App\Entity\NormalProduct;
use App\Entity\Product;

class ProductFactory extends ObjectFactory
{

    public function create($type, array $options = [])
    {
        try {
            if (null === $type) {
                $type = NormalProduct::class;
            }
            return $product = new $type();
        } catch (\Exception $exception) {
            echo "Cannot create product $type";
        }
    }
}